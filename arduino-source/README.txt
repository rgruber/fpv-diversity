Copy the "diversity" folder to the place where all your Arduino sketches are stored.
Then restart your Arduino IDE. The diversity sketch will show up under File->Sketchbook.

Note: Arduino sketches are usually stored under C:\Users\<username>\Documents\Arduino