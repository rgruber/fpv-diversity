/**
    FPV diversity controller project
    Copyright (C) 2014  Robert Gruber

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    
    ------
    
    You can freely download this code from: https://bitbucket.org/rgruber/fpv-diversity/wiki/Home
    Find more info on this project on http://www.rcfan.info
*/
#ifndef __VALUEHOLDER_H__
#define __VALUEHOLDER_H__

#include <Arduino.h>

class ValueHolder
{
public:
  const static int VALUE1 = 0;
  const static int VALUE2 = 1;
  const static int VALUE3 = 2;
  const static int VALUE4 = 3;

  // Instantiate a value holder. Pass how many values should be taken care of
  // and how big the threshold for switching should be (in percent)
  ValueHolder(int numberOfValues, int minSwitchThreshold=5);
  
  // Use these methods to update values inside the holder. The values will
  // be checked and the highest will be stored and can be retreived via the
  // getHigher() method. The logic will only with to a high value if it is
  // at least "minSwitchThreshold" percent high than the value that was the
  // highest during the last update.
  // Returns true if the new values lead to a change which one is the new highest
  boolean update(int v1, int v2);
  boolean update(int v1, int v2, int v3);
  boolean update(int v1, int v2, int v3, int v4);
  
  // Use this method to retrive the value that was the highest during the last
  // update. Note that this does not return the actual value but one of the upper
  // defined constants that indicate which of the given values was the highest.
  int getHigher();

private:
  void recordMinMax(int valueID, int value);
  int mapValue(int valueID, int v1, int v2, int v3, int v4);
  int getValue(int valueID, int v1, int v2, int v3, int v4);

  int curHighValue;
  int lowValue[4];
  int maxValue[4];
  int threshold;
  int numValues;
};

ValueHolder::ValueHolder(int num, int t)
{
  numValues = num;
  threshold = t;
  curHighValue = VALUE1;
}

boolean ValueHolder::update(int v1, int v2)
{
  return update(v1, v2, 0, 0);
}

boolean ValueHolder::update(int v1, int v2, int v3)
{
  return update(v1, v2, v3, 0);
}

boolean ValueHolder::update(int v1, int v2, int v3, int v4)
{
  // the values come from the ADC, so they must always be within 
  // this range. otherwise something went totally wrong...
  v1 = constrain(v1, 0, 1023);
  v2 = constrain(v2, 0, 1023);
  v3 = constrain(v3, 0, 1023);
  v4 = constrain(v4, 0, 1023);

  // record all time low and max values
  recordMinMax(VALUE1, v1);
  recordMinMax(VALUE2, v2);
  recordMinMax(VALUE3, v3);
  recordMinMax(VALUE4, v4);

  // map the current value to a percentage based on
  // the recorded low and max values
  int maxV = mapValue(curHighValue, v1,v2,v3,v4);
  curHighValue = curHighValue;
  boolean newHighValue = false;
  
  for (int i=0; i<numValues; i++) {
    int tmp = mapValue(i, v1,v2,v3,v4);
    if ((tmp - maxV) > threshold) {
      // only if this value is higher and exceeds the threshold 
      // we'll switch to the new highest value
      maxV = tmp;
      curHighValue = i;
      newHighValue=true;
    }
  }
  return newHighValue;
}

void ValueHolder::recordMinMax(int valueID, int value)
{
  lowValue[valueID] = min(lowValue[valueID], value);
  maxValue[valueID] = max(maxValue[valueID], value);
}

int ValueHolder::mapValue(int valueID, int v1, int v2, int v3, int v4)
{
  int v = getValue(valueID, v1, v2, v3, v4);
  return map(v, lowValue[valueID], maxValue[valueID], 0, 100);
}

int ValueHolder::getValue(int valueID, int v1, int v2, int v3, int v4)
{
  int ret = 0;
  switch (valueID) {
    case VALUE1: ret=v1; break;
    case VALUE2: ret=v2; break;
    case VALUE3: ret=v3; break;
    case VALUE4: ret=v4; break;
    default: break;
  }
  return ret;
}

int ValueHolder::getHigher()
{
  return curHighValue;
}

#endif

