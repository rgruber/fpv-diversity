/**
    FPV diversity controller project
    Copyright (C) 2014  Robert Gruber

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    
    ------
    
    You can download this code for free from: https://bitbucket.org/rgruber/fpv-diversity/wiki/Home
    Find more info on this project on http://www.rcfan.info
*/

#include "valueholder.h"

// comment next line if building for an Arduino
#define ATTINY

#ifdef ATTINY
//           _______
//          |o      |
//    RESET |   T   | VCC
//       A3 |   I   | A2
//       A4 |   N   | D1
//      GND |   Y   | D0
//          |_______| 
int pinLed1 = 0;
int pinLed2 = 1;
int pinIn1 = 3;
int pinIn2 = 2;

#else // ARDUINO UNO
int pinLed1 = 8; //D8
int pinLed2 = 9; //D9
int pinIn1 = 0; //A0
int pinIn2 = 1; //A1
#endif

boolean firstRun=true;

ValueHolder h = ValueHolder(2,5);


void setup() {
#ifndef ATTINY
  Serial.begin(9600);
  Serial.println("Starting diversity:");
#endif

  // initialize the digital pins as an output
  pinMode(pinLed1, OUTPUT);
  pinMode(pinLed2, OUTPUT);
  digitalWrite(pinLed1, LOW);
  digitalWrite(pinLed2, LOW);

  // DEBUG CODE -- BEGIN --
  // this forces both values to the max range
  // for the future here should be some kind of setup mechanism
  h.update(0,0);
  h.update(1023, 1023);
  // DEBUG CODE -- END --
}

void loop() {
  // read the RSSI input values
  int val1 = analogRead(pinIn1);
  int val2= analogRead(pinIn2);


  // DEBUG CODE -- BEGIN --
#ifndef ATTINY
  Serial.print("P1:");
  Serial.println(val1);
  Serial.print("P2:");
  Serial.println(val2);
  Serial.println();
#endif
  // DEBUG CODE -- END --


  // pass the new values to the ValueHolder
  boolean changed = h.update(val1, val2);
  // and check if the new values result in a change
  if (changed || firstRun) {
    firstRun=false;
    
#ifndef ATTINY
    Serial.println("High-Value changed!");
#endif    
    
    // disable all ...
    digitalWrite(pinLed1, LOW);
    digitalWrite(pinLed2, LOW);
  
    // ... then switch to what ever input has the higher value
    switch(h.getHigher()) {
      case ValueHolder::VALUE1:
        digitalWrite(pinLed1, HIGH);
        break;
      case ValueHolder::VALUE2:
        digitalWrite(pinLed2, HIGH);
        break;
    }
  }
  
  delay(500);
}

